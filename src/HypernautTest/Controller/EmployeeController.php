<?php

namespace HypernautTest\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EmployeeController extends Controller
{
    /**
     * @Route("/employe/all", )
     */
    public function all(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('employee/all.html.twig');
    }
}
