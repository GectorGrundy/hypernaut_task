#!/bin/bash

sudo usermod -aG docker $(whoami)
docker kill $(docker ps -q)

sudo rm -Rf var/logs/
sudo rm -Rf var/cache/
mkdir docker_log
sudo chmod -R 0777 var/logs/
sudo chmod -R 0777 var/cache/

cd docker
docker-compose down
docker-compose build --force-rm
docker-compose up -d &

