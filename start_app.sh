#!/bin/bash

docker exec hypernaut_php rm -Rf /var/www/symfony/var/logs/*
docker exec hypernaut_php rm -Rf /var/www/symfony/var/cache/*

docker exec hypernaut_php chmod -Rf 0777 /var/www/symfony
docker exec hypernaut_php mkdir /var/www/symfony/bundles
docker exec hypernaut_php chown -Rf www-data:www-data /var/www/symfony/bundles

docker exec hypernaut_php mkdir /var/www/.composer
docker exec hypernaut_php chmod -R 0777 /var/www/.composer

docker exec hypernaut_db mkdir /home/db_structure

docker exec hypernaut_php chown www-data:www-data /var/www/.composer

docker exec hypernaut_db sudo -u  postgres psql -c"CREATE DATABASE hypernaut;"
docker exec hypernaut_db sudo -u  postgres psql -c"CREATE USER hypernaut WITH password 'hypernaut';"
docker exec hypernaut_db sudo -u  postgres psql -c"GRANT ALL ON DATABASE hypernaut TO hypernaut;"

docker exec hypernaut_php sudo -u www-data composer install  --optimize-autoloader

docker exec hypernaut_php chmod -Rf 0777 /var/www/symfony

docker exec -u www-data hypernaut_php php app/console -e=dev doctrine:migrations:migrate --force
